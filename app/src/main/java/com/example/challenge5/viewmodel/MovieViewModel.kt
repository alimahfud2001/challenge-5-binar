package com.example.challenge5.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challenge5.model.GetMovieResponse
import com.example.challenge5.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.example.challenge5.model.GetMovieResponseItem
import kotlinx.android.synthetic.main.fragment_home.*

class MovieViewModel: ViewModel() {
    var movieList = MutableLiveData<List<GetMovieResponseItem>>()
    var code: Int? = null

    fun fetchAllData() {
        ApiClient.instance.getTopRated()
            .enqueue(object : Callback<GetMovieResponse> {
                override fun onResponse(
                    call: Call<GetMovieResponse>,
                    response: Response<GetMovieResponse>
                ) {
                    movieList.postValue(response.body()?.results)
                    code = response.code()
                }

                override fun onFailure(call: Call<GetMovieResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

}