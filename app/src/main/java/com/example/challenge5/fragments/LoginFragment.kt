package com.example.challenge5.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.findNavController
import com.example.challenge5.R
import com.example.challenge5.databinding.FragmentLoginBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "sharedpreferenceschallenge5"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val gson = Gson()

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finish()
            }
        })

        binding.btnLogin.setOnClickListener {
            val email = binding.ti1.editText?.text.toString()
            val password = binding.ti2.editText?.text.toString()
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            val json  = sharedPreferences.getString(email, null)
            val set = gson.fromJson<ArrayList<String>>(json, object : TypeToken<ArrayList<String>>(){}.type)
            if (email.isEmpty()||password.isEmpty())
                Toast.makeText(requireContext(), "Isi Detail", Toast.LENGTH_SHORT).show()
            else {
                if (set?.elementAt(1) == null)
                    Toast.makeText(requireContext(), "Email Tidak Terdaftar", Toast.LENGTH_SHORT).show()
                else{
                    if (password != set.elementAt(2))
                        Toast.makeText(requireContext(), "Email/Password Salah", Toast.LENGTH_SHORT).show()
                    else {
                        editor.putString("isLoggedIn", email)
                        editor.apply()
                        it.findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                    }
                }
            }
        }

        binding.tvblmpnya.setOnClickListener{
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}