package com.example.challenge5.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.example.challenge5.R
import com.example.challenge5.adapter.MovieAdapter
import com.example.challenge5.databinding.FragmentHomeBinding
import com.example.challenge5.model.GetMovieResponseItem
import com.example.challenge5.viewmodel.MovieViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MovieViewModel by viewModels()
    private val sharedPrefFile = "sharedpreferenceschallenge5"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
         }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val gson = Gson()
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val email = sharedPreferences.getString("isLoggedIn", null)
        val json = sharedPreferences.getString(email, null)
        val set = gson.fromJson<ArrayList<String>>(json, object : TypeToken<ArrayList<String>>(){}.type)

        binding.tvHomeHello.setText("Hi, ${set?.elementAt(0)}")
        viewModel.movieList.observe(requireActivity(), Observer {
            if (viewModel.code == 200){
                showList(it)
                binding.progressBar.visibility = View.GONE
            }
            else binding.progressBar.visibility = View.GONE
        })
        viewModel.fetchAllData()

        binding.ivHomeToprofil.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }
    }

    private fun showList(data: List<GetMovieResponseItem>?){
        val adapter = MovieAdapter(requireContext())
        adapter.submitData(data)
        binding.recyclerview.setLayoutManager(GridLayoutManager(requireContext(), 3))
        binding.recyclerview.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}