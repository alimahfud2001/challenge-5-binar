package com.example.challenge5.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import com.example.challenge5.R
import com.example.challenge5.databinding.FragmentProfileBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_logout_dialog.view.*
import okhttp3.internal.wait


class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "sharedpreferenceschallenge5"

   override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       _binding = FragmentProfileBinding.inflate(inflater, container, false)
       return binding.root
   }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val email = sharedPreferences.getString("isLoggedIn", null)
        var json = sharedPreferences.getString(email, null)
        val gson = Gson()
        val set = gson.fromJson<ArrayList<String>>(json, object : TypeToken<ArrayList<String>>(){}.type)

        loadData(set)

        binding.btnSave.setOnClickListener {
            if (!binding.et1.text.isNullOrEmpty()|| !binding.et2.text.isNullOrEmpty()||
                !binding.et3.text.isNullOrEmpty()|| !binding.et4.text.isNullOrEmpty()){
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                val _set = arrayListOf(binding.et1.text.toString(), binding.et2.text.toString(), set.elementAt(2), binding.et3.text.toString(), binding.et4.text.toString())
                val jsonPut = gson.toJson(_set)
                if (email != binding.et2.text.toString()) {
                    editor.remove(email)
                }
                editor.putString(binding.et2.text.toString(), jsonPut)
                editor.putString("isLoggedIn", binding.et2.text.toString())
                editor.apply()

                Toast.makeText(requireContext(), "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show()
            }
            else Toast.makeText(requireContext(), "Isi Semua Data", Toast.LENGTH_SHORT).show()
        }

        binding.btnLogout.setOnClickListener {
            val view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_logout_dialog, null, false)
            val dialogBuilder = AlertDialog.Builder(requireContext())
            dialogBuilder.setView(view)
            val dialog = dialogBuilder.create()
            dialog.show()
            view.btndeleteyes.setText("Logout")
            view.tvdelete.setText("Logout Akun?")
            val _it = it
            view.btndeleteyes.setOnClickListener {
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putStringSet("isLoggedIn", null)
                editor.apply()
                _it.findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
                dialog?.dismiss()
            }
            view.btndeleteno.setOnClickListener {
                dialog?.dismiss()
            }
        }
    }

    private fun loadData(set: ArrayList<String>) {
        binding.et1.setText(set.elementAt(0))
        binding.et2.setText(set.elementAt(1))

        if (set.size >= 5){
            binding.et3.setText(set.elementAt(3))
            binding.et4.setText(set.elementAt(4))
        }
    }
}