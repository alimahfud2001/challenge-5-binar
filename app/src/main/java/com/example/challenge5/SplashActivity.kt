package com.example.challenge5

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.example.challenge5.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {
    private val sharedPrefFile = "sharedpreferenceschallenge5"
    private lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val isLoggedIn: String? = sharedPreferences.getString("isLoggedIn", null)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            when (isLoggedIn){
                null -> MainActivity.open(this, null)
                else -> MainActivity.open(this, isLoggedIn)
            }
            finish()
        }, 2000)
    }
}